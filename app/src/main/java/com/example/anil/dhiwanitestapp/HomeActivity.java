package com.example.anil.dhiwanitestapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.example.anil.dhiwanitestapp.CustomViews.DatePickerView;
import com.example.anil.dhiwanitestapp.CustomViews.DatePickerView.DatePickerListener;
import com.example.anil.dhiwanitestapp.CustomViews.EditTextWithNumber;
import com.example.anil.dhiwanitestapp.CustomViews.EditTextWithString;
import com.example.anil.dhiwanitestapp.CustomViews.MultipleDropDown;
import com.example.anil.dhiwanitestapp.CustomViews.SingleDropDown;
import com.example.anil.dhiwanitestapp.api.ApiClient;
import com.example.anil.dhiwanitestapp.api.ApiInterface;
import com.example.anil.dhiwanitestapp.model.MainModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements
        EditTextWithString.EditTextStringListener, EditTextWithNumber.EditTextNumberListener
  , SingleDropDown.DropDownListener, MultipleDropDown.MultipleDropdownListener,
        DatePickerListener{

    private LinearLayout llMainLayout;
    private Context context;
    private Map<String, String> showDataModelList = new HashMap<>();

    private Button btnSubmit;
    private ProgressBar progressBar;
    private ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        llMainLayout = findViewById(R.id.ll_main_layout);
        btnSubmit = findViewById(R.id.btn_submit);
        progressBar = findViewById(R.id.progressBar);
        scrollView = findViewById(R.id.scrollView);
        context = this;

        ApiInterface apiInterface1 = ApiClient.getClient().create(ApiInterface.class);
        Call<MainModel> call1 = apiInterface1.getCustomLayoutData();
        call1.enqueue(new Callback<MainModel>() {
            @Override
            public void onResponse(Call<MainModel> call, Response<MainModel> response) {
                inflateCustomUi(response.body());
            }

            @Override
            public void onFailure(Call<MainModel> call, Throwable t) {
                Log.d("api_response", "response in failure" + t.getLocalizedMessage());
            }
        });

        onClickSubmit();
    }

    ArrayList<String> dataList = new ArrayList<>();
    private void onClickSubmit() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Map.Entry<String, String> entry : showDataModelList.entrySet()) {
                    dataList.add(entry.getValue());
                }

                if (dataList.size() > 0) {
                    Intent intent = new Intent(HomeActivity.this, ShowDataActivity.class);
                    intent.putStringArrayListExtra("data", dataList);
                    startActivity(intent);
                }
            }
        });
    }

    /**
     * inflating the custom ui according to data
     *
     * @param response
     */
    private void inflateCustomUi(final MainModel response) {

        progressBar.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
        for (int i = 0; i < response.getData().size(); i++) {
            switch (response.getData().get(i).getType()) {
                case Constants.EDIT_TEXT_STRING:
                    EditTextWithString editTextWithString = new EditTextWithString(this);
                    editTextWithString.setData(response.getData().get(i));
                    editTextWithString.setEditTextStringListener(this);
                    llMainLayout.addView(editTextWithString.getInflatedView());
                    break;
                case Constants.EDIT_TEXT_INTEGER:
                    EditTextWithNumber editTextWithNumber = new EditTextWithNumber(this);
                    editTextWithNumber.setData(response.getData().get(i));
                    editTextWithNumber.setEditTextNumberListener(this);
                    llMainLayout.addView(editTextWithNumber.getInflatedView());
                    break;
                case Constants.DROPDOWN:
                    SingleDropDown singleDropDown = new SingleDropDown(this);
                    singleDropDown.setData(response.getData().get(i));
                    singleDropDown.setDropDownListener(this);
                    llMainLayout.addView(singleDropDown.getInflatedView());
                    break;
                case Constants.MULTISELECT:
                    MultipleDropDown multipleDropDown = new MultipleDropDown(this);
                    multipleDropDown.setData(response.getData().get(i));
                    multipleDropDown.setMultipleDropdownListener(this);
                    llMainLayout.addView(multipleDropDown.getInflatedView());
                    break;
                case Constants.DATE:
                    DatePickerView datePickerView = new DatePickerView(this);
                    datePickerView.setData(response.getData().get(i));
                    datePickerView.setFragmentManager(getSupportFragmentManager());
                    datePickerView.setDatePickerListener(this);
                    llMainLayout.addView(datePickerView.getInflatedView());
                    break;
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        showDataModelList.clear();
    }

    // callbacks for compound views

    @Override
    public void getEditTextData(String key, String etString) {
        showDataModelList.put(key, etString);
    }

    @Override
    public void removeEditTextData(String key) {
        showDataModelList.remove(key);
    }

    @Override
    public void getSpinnerData(String key, String option) {
        showDataModelList.put(key, option);
    }

    @Override
    public void removeSpinnerData(String key) {
        showDataModelList.remove(key);
    }

    @Override
    public void getEditTextNumberData(String key, String etString) {
        showDataModelList.put(key, etString);
    }

    @Override
    public void removeEditTextNumberData(String key) {
        showDataModelList.remove(key);
    }

    @Override
    public void getMultipleData(String key, String value) {
        showDataModelList.put(key, value);
    }

    @Override
    public void getDateData(String key, String date) {
        showDataModelList.put(key, date);
    }

    @Override
    public void removeDateData(String key) {
        showDataModelList.remove(key);
    }
}
