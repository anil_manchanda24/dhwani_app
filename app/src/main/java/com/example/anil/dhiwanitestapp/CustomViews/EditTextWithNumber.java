package com.example.anil.dhiwanitestapp.CustomViews;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.anil.dhiwanitestapp.R;
import com.example.anil.dhiwanitestapp.model.Data;

public class EditTextWithNumber extends LinearLayout {

    private Data data;
    private Context context;
    private EditTextNumberListener listener;

    public EditTextWithNumber(Context context) {
        super(context);
        init(context);
    }

    public EditTextWithNumber(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public EditTextWithNumber(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public EditTextWithNumber(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void init(Context context) {
        this.setOrientation(VERTICAL);
        this.context = context;
    }

    public void setData(Data data) {
        this.data = data;
        customViewUi();
    }

    public void setEditTextNumberListener(EditTextNumberListener listener) {
        this.listener = listener;
    }

    View inflatedLayout;
    public void customViewUi() {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflatedLayout = inflater.inflate(R.layout.layout_edit_text_number, this, true);
        TextView tvLabel = inflatedLayout.findViewById(R.id.tv_number);
        TextView tvName = inflatedLayout.findViewById(R.id.tv_name);
        EditText etNumber = inflatedLayout.findViewById(R.id.et_number);

        tvLabel.setText(data.getLabel());
        tvName.setText(data.getName());
        etNumber.setTag(data.getId());
        editTextCallBack(tvLabel, etNumber);
    }

    public View getInflatedView() {
        return inflatedLayout;
    }

    /**
     * method to add callback to edittext
     *
     * @param tvLabel
     * @param etNumber
     */
    public void editTextCallBack(final TextView tvLabel, final EditText etNumber) {

        etNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    tvLabel.setBackgroundResource(R.drawable.circle_color_bg);
                    listener.getEditTextNumberData(etNumber.getTag().toString(), s.toString());
                } else {
                    tvLabel.setBackgroundResource(R.drawable.circle_bg);
                    listener.removeEditTextNumberData(etNumber.getTag().toString());
                }
            }
        });
    }

    public interface EditTextNumberListener {
        void getEditTextNumberData(String key, String etString);
        void removeEditTextNumberData(String key);
    }
}
