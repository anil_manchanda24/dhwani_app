package com.example.anil.dhiwanitestapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.anil.dhiwanitestapp.model.MultipleSelectModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MultipleListViewAdapter extends BaseAdapter {

    public Context context;
    public List<MultipleSelectModel> list;
    private LayoutInflater inflator;
    CheckboxListener checkboxDataListener;

    public MultipleListViewAdapter(Context context, List<MultipleSelectModel> list) {
        this.context = context;
        this.list = list;
        inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setCheckboxDataListener(CheckboxListener listener) {
        checkboxDataListener = listener;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    Map<Integer, String> mapList = new HashMap<>();
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflator.inflate(R.layout.layout_multiple_dropdown, null);
            holder = new ViewHolder();
            holder.chk = convertView.findViewById(R.id.checkbox_data);
            holder.chk.setTag(position);
            holder.chk.setText(list.get(position).getValue());
            holder.chk
                    .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton view,
                                                     boolean isChecked) {
                            int getPosition = (Integer) view.getTag();
                            if (isChecked) {
                                list.get(getPosition).setSelected(view.isChecked());
                                mapList.put(getPosition, list.get(getPosition).getValue());
                            } else {
                                list.get(getPosition).setSelected(view.isChecked());
                                mapList.remove(getPosition);
                            }

                            checkboxDataListener.getCheckboxData(mapList);
                        }
                    });

            convertView.setTag(holder);
            convertView.setTag(R.id.checkbox, holder.chk);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.chk.setTag(position);

        holder.chk.setChecked(list.get(position).isSelected());

        return convertView;
    }

    static class ViewHolder {
        protected CheckBox chk;
    }

    public interface CheckboxListener {
        void getCheckboxData(Map<Integer, String> mapList);
    }
}
