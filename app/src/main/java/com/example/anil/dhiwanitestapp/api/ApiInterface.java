package com.example.anil.dhiwanitestapp.api;

import com.example.anil.dhiwanitestapp.model.Data;
import com.example.anil.dhiwanitestapp.model.MainModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("task/new-task")
    Call<MainModel> getCustomLayoutData();
}
