package com.example.anil.dhiwanitestapp.model;

import java.util.List;

public class MainModel {
    private String status;

    private List<Data> data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public List<Data> getData() {
        return data;
    }
}

