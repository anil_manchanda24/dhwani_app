package com.example.anil.dhiwanitestapp.api;

import android.util.Log;

import com.example.anil.dhiwanitestapp.BuildConfig;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static final String BASE_URL = "http://stgdreamafrica.dhwaniris.in/index.php/v1/";
    public static final String HEADER_VALUE = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwIiwidXNlcl9sZXZlbCI6IjIiLCJuYW1lIjoiIiwibW9iaWxlX251bWJlciI6IiIsInBhc3N3b3JkX3Jlc2V0X2F0IjpudWxsLCJ2ZXJzaW9uIjpudWxsLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6bnVsbCwiY3JlYXRlZF9hdCI6IjIwMTctMTItMTEgMTg6MTc6NTkiLCJ1c2VyX2lkIjoiMSIsInNjaG9vbF9pZCI6bnVsbCwiY291bnRyeV9pZCI6IjEiLCJsb2dpbl9pZCI6Nzh9.zdMb7fpWaDjFvizMGuxd1UINc0tLfEk7S6a25WoX4qA";
    private static Retrofit retrofit = null;

    static Gson gson = new GsonBuilder()
            .setLenient()
            .create();


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(getHeader())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public static OkHttpClient getHeader() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("token", HEADER_VALUE)
                        .method(original.method(), original.body())
                        .build();

                Log.e("interceptor" , " value = " + chain.proceed(request).body().toString());
                return chain.proceed(request);
            }
        });

        httpClient.connectTimeout(30, TimeUnit.SECONDS);
        httpClient.readTimeout(30, TimeUnit.SECONDS);
        httpClient.writeTimeout(30, TimeUnit.SECONDS);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.interceptors().add(interceptor);

        httpClient.interceptors().add(new LogJsonInterceptor());
        OkHttpClient client = httpClient.build();
        return client;
    }

    public static class LogJsonInterceptor implements Interceptor {
        @Override
        public Response intercept(Interceptor.Chain chain) throws IOException {
            Request request = chain.request();

            Response response = chain.proceed(request);
            String rawJson = response.body().string();

            Log.d("log_interceptor", String.format("raw JSON response is: %s", rawJson));

            Log.e("log_interceptor", "formatted string = " + rawJson.substring(5));
            // Re-create the response before returning it because body can be read only once
            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), rawJson.substring(5))).build();
        }
    }
}
