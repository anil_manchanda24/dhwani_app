package com.example.anil.dhiwanitestapp.CustomViews;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.anil.dhiwanitestapp.DatePickerFragment;
import com.example.anil.dhiwanitestapp.R;
import com.example.anil.dhiwanitestapp.model.Data;


public class EditTextWithString extends LinearLayout {

    private Context context;
    private Data data;
    private DatePickerFragment datePickerFragment;
    private EditTextStringListener listener;

    public EditTextWithString(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public EditTextWithString(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public EditTextWithString(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public EditTextWithString(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        init();
    }

    public void setData(Data data) {
        this.data = data;
        customViewUi();
    }

    public void init() {
        this.setOrientation(VERTICAL);
    }

    public void setEditTextStringListener(EditTextStringListener listener) {
        this.listener = listener;
    }

    View inflatedLayout;
    public void customViewUi() {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflatedLayout = inflater.inflate(R.layout.layout_edit_text_string, this, true);
        TextView tvLabel = inflatedLayout.findViewById(R.id.tv_number);
        TextView tvName = inflatedLayout.findViewById(R.id.tv_name);
        EditText etText = inflatedLayout.findViewById(R.id.et_string);

        Log.e("value", " value of data passed inside = " + data.getName() + " label = " + data.getLabel());
        tvLabel.setText(data.getLabel());
        tvName.setText(data.getName());
        etText.setTag(data.getId());
        editTextCallBack(tvLabel, etText);
    }

    public View getInflatedView() {
        return inflatedLayout;
    }

    /**
     * method to add callback to edittext
     *
     * @param tvLabel
     * @param etText
     */
    public void editTextCallBack(final TextView tvLabel, final EditText etText) {

        etText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    tvLabel.setBackgroundResource(R.drawable.circle_color_bg);
                    listener.getEditTextData(etText.getTag().toString(), s.toString());
                } else {
                    tvLabel.setBackgroundResource(R.drawable.circle_bg);listener.removeEditTextData(etText.getTag().toString());
                }
            }
        });
    }

    public interface EditTextStringListener {
        void getEditTextData(String key, String etString);
        void removeEditTextData(String key);
    }
}
