package com.example.anil.dhiwanitestapp.CustomViews;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.anil.dhiwanitestapp.MultipleListViewAdapter;
import com.example.anil.dhiwanitestapp.R;
import com.example.anil.dhiwanitestapp.model.Data;
import com.example.anil.dhiwanitestapp.model.MainModel;
import com.example.anil.dhiwanitestapp.model.MultipleSelectModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class MultipleDropDown extends LinearLayout {

    private Context context;
    private Data data;
    private List<String> selectedItems = new ArrayList<>();
    private MultipleDropdownListener multipleDropdownListener;

    public MultipleDropDown(Context context) {
        super(context);
        init(context);
    }

    public MultipleDropDown(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MultipleDropDown(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MultipleDropDown(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void setData(Data data) {
        this.data = data;
        customViewUi();
    }

    public void init(Context context) {
        this.setOrientation(VERTICAL);
        this.context = context;
    }

    public void setMultipleDropdownListener(MultipleDropdownListener listener) {
        this.multipleDropdownListener = listener;
    }

    private View inflatedLayout;

    public void customViewUi() {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflatedLayout = inflater.inflate(R.layout.layout_multiple, this, true);
        TextView tvLabel = inflatedLayout.findViewById(R.id.tv_number);
        TextView tvName = inflatedLayout.findViewById(R.id.tv_name);
        TextView tvMultiple = inflatedLayout.findViewById(R.id.tv_multiple);

        tvLabel.setText(data.getLabel());
        tvName.setText(data.getName());
        tvMultiple.setTag(data.getId());
        tvMultiple.setText("Select");
        multipleDropDownClick(data, tvMultiple, tvLabel);
    }

    public View getInflatedView() {
        return inflatedLayout;
    }

    /**
     * method to show multiple selection dialog
     *
     * @param tvMultiple
     */
    private void multipleDropDownClick(final Data data,
                                       final TextView tvMultiple, final TextView tvLabel) {

        tvMultiple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMultipleDialog(data, tvMultiple, tvLabel);
            }
        });
    }

    /**
     * method to show multiple selection dialog
     *
     * @param response
     * @param position
     */
    private List<MultipleSelectModel> optionsData = new ArrayList<>();

    public void showMultipleDialog(Data data,
                                   final TextView tvMultiple, final TextView tvLabel) {
        final Dialog dialog = new Dialog(context);

        dialog.setContentView(R.layout.dialog_layout);
        TextView tvOk = dialog.findViewById(R.id.tv_ok);
        ListView listView = dialog.findViewById(R.id.listView);

        if (optionsData.isEmpty()) {
            for (int i = 0; i < data.getOptions().size(); i++) {
                MultipleSelectModel multipleSelectModel = new MultipleSelectModel();
                multipleSelectModel.setId(data.getOptions().get(i).getId());
                multipleSelectModel.setValue(data
                        .getOptions().get(i).getValue());
                optionsData.add(multipleSelectModel);
            }
        } else {
            Log.e("in else", " do nothing for options");
        }

        final MultipleListViewAdapter.CheckboxListener listener = new MultipleListViewAdapter.CheckboxListener() {
            @Override
            public void getCheckboxData(Map<Integer, String> mapList) {
                if (mapList.size() > 0) {
                    selectedItems = new ArrayList<>();
                    for (Map.Entry<Integer, String> entry : mapList.entrySet()) {
                        Log.e("selected", " selected data = " + entry.getValue());
                        selectedItems.add(entry.getValue());
                        tvLabel.setBackgroundResource(R.drawable.circle_color_bg);
                    }
                    String selectedString = selectedItems.toString().substring(1);
                    String finalString = selectedString.substring(0, selectedString.length() - 1);
                    tvMultiple.setText(finalString);
                    multipleDropdownListener.getMultipleData(tvMultiple.getTag().toString(), finalString);
                } else {
                    tvMultiple.setText("Select");
                    selectedItems = new ArrayList<>();
                    tvLabel.setBackgroundResource(R.drawable.circle_bg);
                }
            }
        };

        MultipleListViewAdapter adapter = new MultipleListViewAdapter(context, optionsData);
        adapter.setCheckboxDataListener(listener);
        listView.setAdapter(adapter);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public interface MultipleDropdownListener {
        void getMultipleData(String key, String value);
    }
}
