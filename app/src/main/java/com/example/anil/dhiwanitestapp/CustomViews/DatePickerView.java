package com.example.anil.dhiwanitestapp.CustomViews;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.anil.dhiwanitestapp.DatePickerFragment;
import com.example.anil.dhiwanitestapp.R;
import com.example.anil.dhiwanitestapp.model.Data;


public class DatePickerView extends LinearLayout {

    private Context context;
    private Data data;
    private FragmentManager fragmentManager;
    private DatePickerListener datePickerListener;

    public DatePickerView(Context context) {
        super(context);
        init(context);
    }

    public DatePickerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DatePickerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public DatePickerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        this.setOrientation(VERTICAL);
        this.context = context;
    }

    public void setData(Data data) {
        this.data = data;
        customViewUi();
    }

    public void setDatePickerListener(DatePickerListener listener) {
         this.datePickerListener = listener;
    }

    View inflatedLayout;
    public void customViewUi() {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflatedLayout = inflater.inflate(R.layout.layout_date_picker, this, true);
        TextView tvLabel = inflatedLayout.findViewById(R.id.tv_number);
        TextView tvName = inflatedLayout.findViewById(R.id.tv_name);
        TextView tvDatePicker = inflatedLayout.findViewById(R.id.tv_date_picker);

        Log.e("value", " value of data passed inside = " + data.getName() + " label = " + data.getLabel());
        tvLabel.setText(data.getLabel());
        tvName.setText(data.getName());
        tvDatePicker.setTag(data.getId());
        showDatePicker(tvDatePicker, tvLabel);
    }

    public View getInflatedView() {
        return inflatedLayout;
    }

    public void setFragmentManager(FragmentManager manager) {
        this.fragmentManager = manager;
    }

    /**
     * method to show date picker
     *
     * @param tvDatePicker
     * @param tvLabel
     */
    private void showDatePicker(final TextView tvDatePicker, final TextView tvLabel) {

        final DatePickerFragment.DateValueListener listener = new DatePickerFragment.DateValueListener() {
            @Override
            public void getSelectedDate(String date) {
                tvDatePicker.setText(date);
                if (!TextUtils.isEmpty(date)) {
                    tvLabel.setBackgroundResource(R.drawable.circle_color_bg);
                    datePickerListener.getDateData(tvDatePicker.getTag().toString(), date);
                } else {
                    tvLabel.setBackgroundResource(R.drawable.circle_bg);
                    datePickerListener.removeDateData(tvDatePicker.getTag().toString());
                }
            }
        };

        tvDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.setDateValueListener(listener);
                datePickerFragment.show(fragmentManager, "date picker");
            }
        });
    }

    public interface DatePickerListener {
        void getDateData(String key, String date);
        void removeDateData(String key);
    }
}
