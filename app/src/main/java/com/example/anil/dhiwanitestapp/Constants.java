package com.example.anil.dhiwanitestapp;

public class Constants {

    public static final String EDIT_TEXT_STRING = "1";
    public static final String EDIT_TEXT_INTEGER = "2";
    public static final String DROPDOWN = "3";
    public static final String MULTISELECT = "4";
    public static final String DATE = "5";
}
