package com.example.anil.dhiwanitestapp;


import android.os.Parcel;
import android.os.Parcelable;

class ShowDataModel implements Parcelable {

    private String type;
    private String value;

    protected ShowDataModel(Parcel in) {
        type = in.readString();
        value = in.readString();
    }

    public ShowDataModel() {
    }

    public static final Creator<ShowDataModel> CREATOR = new Creator<ShowDataModel>() {
        @Override
        public ShowDataModel createFromParcel(Parcel in) {
            return new ShowDataModel(in);
        }

        @Override
        public ShowDataModel[] newArray(int size) {
            return new ShowDataModel[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(value);
    }
}
