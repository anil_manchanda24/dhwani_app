package com.example.anil.dhiwanitestapp.CustomViews;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.anil.dhiwanitestapp.R;
import com.example.anil.dhiwanitestapp.model.Data;
import com.example.anil.dhiwanitestapp.model.MainModel;

import java.util.ArrayList;
import java.util.List;


public class SingleDropDown extends LinearLayout {

    private Context context;
    private Data data;
    private DropDownListener listener;

    public SingleDropDown(Context context) {
        super(context);
        init(context);
    }

    public SingleDropDown(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SingleDropDown(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SingleDropDown(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        this.setOrientation(VERTICAL);
    }

    public void setData(Data data) {
        this.data = data;
        customViewUi();
    }

    View inflatedLayout;
    public void customViewUi() {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflatedLayout = inflater.inflate(R.layout.layout_single_dropdown, this, true);
        TextView tvLabel = inflatedLayout.findViewById(R.id.tv_number);
        TextView tvName = inflatedLayout.findViewById(R.id.tv_name);
        Spinner spinner = inflatedLayout.findViewById(R.id.spinner_dropdown);

        tvLabel.setText(data.getLabel());
        tvName.setText(data.getName());
        spinner.setTag(data.getId());
        spinnerDropDownData(spinner, data, tvLabel);
    }

    public View getInflatedView() {
        return inflatedLayout;
    }

    public void setDropDownListener(DropDownListener listener) {
        this.listener = listener;
    }

    /**
     * method to add data to spinner
     *
     * @param spinner
     * @param tvLabel
     */

    public void spinnerDropDownData(final Spinner spinner, Data data, final TextView tvLabel) {
        final List<String> dropdownData = new ArrayList<>();
            for (int j = 0; j < data.getOptions().size(); j++) {
                dropdownData.add(data.getOptions().get(j).getValue());
            }

            dropdownData.add(0, "Select");

        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1, dropdownData);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adp1);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    tvLabel.setBackgroundResource(R.drawable.circle_color_bg);
                    listener.getSpinnerData(spinner.getTag().toString(), dropdownData.get(position));

                } else {
                    tvLabel.setBackgroundResource(R.drawable.circle_bg);
                    listener.removeSpinnerData(spinner.getTag().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    public interface DropDownListener {
        void getSpinnerData(String key, String option);
        void removeSpinnerData(String key);
    }
}
