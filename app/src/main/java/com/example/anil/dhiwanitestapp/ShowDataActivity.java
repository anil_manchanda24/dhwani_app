package com.example.anil.dhiwanitestapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


public class ShowDataActivity extends AppCompatActivity{

    private LinearLayout llMainLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_data_activity);
        llMainLayout = findViewById(R.id.ll_main_layout);

        if (getIntent() != null && !getIntent().getStringArrayListExtra("data").isEmpty()) {
            final TextView[] myTextViews = new TextView[getIntent().getStringArrayListExtra("data").size()];
            for (int i=0;i<getIntent().getStringArrayListExtra("data").size();i++) {
                Log.e("intent","values = " + getIntent().getStringArrayListExtra("data").toString());
                final TextView rowTextView = new TextView(this);
                rowTextView.setText(getIntent().getStringArrayListExtra("data").get(i));

                rowTextView.setPadding(10,10,10,10);
                rowTextView.setTextSize(20);
                llMainLayout.addView(rowTextView);
                myTextViews[i] = rowTextView;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
